TargetFramework = netcoreapp3.1

# Сборка
donet build

# Запуск

dotnet run folder_path destination_path token

folder_path -- путь до папки с файлами

destination_path -- путь до папки в Яндекс Диске. destination_path == "Folder" означает, что Folder находится в корне Яндекс Диска

token -- действительный OAuth-токен для приложения, которое имеет право на запись в любом месте на Яндекс Диске

token можно получить по следующей ссылке: https://oauth.yandex.ru/authorize?response_type=token&client_id=5fa74a4826ad44d0b60560457ce5c723