﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using ConsoleYandexDisk.Exceptions;
using ConsoleYandexDisk.Models;

namespace ConsoleYandexDisk
{
    class YandexDiskClient : IYandexDiskClient
    {
        private readonly HttpClient httpClient;

        public string BaseUrl { get; } = "https://cloud-api.yandex.net/v1/disk/";

        public YandexDiskClient(string token)
        {
            if (token == null || string.IsNullOrEmpty(token))
            {
                throw new ArgumentNullException(nameof(token));
            }

            httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("OAuth", token);
            httpClient.Timeout = TimeSpan.FromHours(2);
            httpClient.BaseAddress = new Uri(BaseUrl);
        }

        private async Task EnsureSuccessStatusCode(HttpResponseMessage response)
        {
            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                var responseBody = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var error = JsonSerializer.Deserialize<ErrorDescription>(responseBody);
                throw new YandexDiskException(error, e);
            }
        }

        public async Task<Link> GetUploadLinkAsync(string path, bool overwrite, CancellationToken cancellationToken)
        {
            var response = await httpClient.GetAsync(
                $"resources/upload?path={path}&overwrite={overwrite}", cancellationToken).ConfigureAwait(false);
            await EnsureSuccessStatusCode(response).ConfigureAwait(false);
            var responseBody = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JsonSerializer.Deserialize<Link>(responseBody);
        }

        public async Task UploadFileAsync(Link link, Stream file, CancellationToken cancellationToken)
        {
            if (link == null)
            {
                throw new ArgumentNullException(nameof(link));
            }

            if (file == null)
            {
                throw new ArgumentNullException(nameof(file));
            }

            var url = new Uri(link.Href);
            var method = new HttpMethod(link.Method);
            var content = new StreamContent(file);
            var request = new HttpRequestMessage(method, url) {Content = content};
            var response = await httpClient.SendAsync(request, cancellationToken).ConfigureAwait(false);
            await EnsureSuccessStatusCode(response).ConfigureAwait(false);
        }

        public async Task<Operation> GetOperationStatus(string operationId, CancellationToken cancellationToken)
        {
            var response = await httpClient.GetAsync($"operations/{operationId}", cancellationToken).ConfigureAwait(false);
            await EnsureSuccessStatusCode(response).ConfigureAwait(false);
            var responseBody = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<Operation>(responseBody);
        }
    }
}
