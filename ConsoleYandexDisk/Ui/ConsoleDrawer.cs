﻿using System;
using System.Collections.Generic;
using System.Threading;
using ConsoleYandexDisk.Models;
using ConsoleYandexDisk.Utils;

namespace ConsoleYandexDisk.Ui
{
    public class ConsoleDrawer: IUploadProgressDrawer
    {
        // fileName -> lineNumber in console window
        private readonly Dictionary<string, int> lineNumbers = new Dictionary<string, int>();

        private readonly object locker = new object();

        private int linesCount;

        public ConsoleDrawer()
        {
            Console.Clear();
        }

        private void WriteLine(string content, int lineNumber)
        {
            lock (locker)
            {
                Console.CursorTop = lineNumber;
                Console.WriteLine(content);
                Console.CursorTop = linesCount;
            }
        }

        private void PrintOperationStatus(string fileName, OperationStatus operationStatus, int lineNumber, ErrorDescription error = null)
        {
            var content = $"[{fileName}] {operationStatus.ToStringRepresentation()}{(error == null ? string.Empty : $" -> Error: {error.Description}")}";
            WriteLine(content.PadRight(Console.WindowWidth, ' '), lineNumber);
        }

        public void SetUploadStatus(string fileName, OperationStatus operationStatus, ErrorDescription error = null)
        {
            if (!lineNumbers.ContainsKey(fileName))
            {
                lock (locker)
                {
                    lineNumbers[fileName] = linesCount;
                }

                Interlocked.Increment(ref linesCount);
            }
            
            var lineNumber = lineNumbers[fileName];

            PrintOperationStatus(fileName, operationStatus, lineNumber, error);
        }
    }
}
