﻿using ConsoleYandexDisk.Models;

namespace ConsoleYandexDisk.Ui
{
    public interface IUploadProgressDrawer
    {
        void SetUploadStatus(string fileName, OperationStatus operationStatus, ErrorDescription error = null);
    }
}
