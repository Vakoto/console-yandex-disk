﻿using System.Text.Json.Serialization;

namespace ConsoleYandexDisk.Models
{
    public class Link
    {
        [JsonPropertyName("operation_id")]
        public string OperationId { get; set; }

        [JsonPropertyName("href")]
        public string Href { get; set; }

        [JsonPropertyName("method")]
        public string Method { get; set; }

        [JsonPropertyName("templated")]
        public bool Templated { get; set; }
    }
}
