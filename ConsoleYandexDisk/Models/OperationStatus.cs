﻿namespace ConsoleYandexDisk.Models
{
    public enum OperationStatus
    {
        Success,
        Failure,
        InProgress
    }
}