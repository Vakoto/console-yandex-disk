﻿using System.Text.Json.Serialization;

namespace ConsoleYandexDisk.Models
{
    public class ErrorDescription
    {
        [JsonPropertyName("description")]
        public string Description { get; set; }

        [JsonPropertyName("error")]
        public string Error { get; set; }
    }
}
