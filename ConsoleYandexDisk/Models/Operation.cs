﻿using System;
using System.Text.Json.Serialization;

namespace ConsoleYandexDisk.Models
{
    public class Operation
    {
        [JsonPropertyName("status")]
        public string Status { get; set; }

        public bool IsCompleted()
        {
            var status = GetStatus();
            return status == OperationStatus.Failure || status == OperationStatus.Success;
        }

        public OperationStatus GetStatus()
        {
            return Status switch
            {
                "success" => OperationStatus.Success,
                "in-progress" => OperationStatus.InProgress,
                "failed" => OperationStatus.Failure,
                _ => throw new ArgumentOutOfRangeException(nameof(Status), Status, null)
            };
        }
    }
}
