﻿using System;
using ConsoleYandexDisk.Models;

namespace ConsoleYandexDisk.Exceptions
{
    public class YandexDiskException: Exception
    {
        public ErrorDescription Error { get; }

        public YandexDiskException(ErrorDescription error, Exception innerException)
            : base(error.Description, innerException)
        {
            Error = error;
        }
    }
}
