﻿using System;
using ConsoleYandexDisk.Models;

namespace ConsoleYandexDisk.Utils
{
    public static class OperationStatusExtensions
    {
        public static string ToStringRepresentation(this OperationStatus operationStatus)
        {
            return operationStatus switch
            {
                OperationStatus.Success => "Success",
                OperationStatus.Failure => "Failure",
                OperationStatus.InProgress => "InProgress",
                _ => throw new ArgumentOutOfRangeException(nameof(operationStatus), operationStatus, null)
            };
        }
    }
}
