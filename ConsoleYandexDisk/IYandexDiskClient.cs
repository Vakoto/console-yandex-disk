﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using ConsoleYandexDisk.Models;

namespace ConsoleYandexDisk
{
    public interface IYandexDiskClient
    {
        Task<Link> GetUploadLinkAsync(string path, bool overwrite, CancellationToken cancellationToken);

        Task UploadFileAsync(Link link, Stream file, CancellationToken cancellationToken);

        Task<Operation> GetOperationStatus(string operationId, CancellationToken cancellationToken);
    }
}
