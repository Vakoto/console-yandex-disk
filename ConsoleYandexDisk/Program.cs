﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ConsoleYandexDisk.Exceptions;
using ConsoleYandexDisk.Models;
using ConsoleYandexDisk.Ui;

namespace ConsoleYandexDisk
{
    class Program
    {
        private static readonly TimeSpan waitingDelay = TimeSpan.FromSeconds(1);

        private static async Task WaitOperationAsync(
            IYandexDiskClient diskClient, IUploadProgressDrawer drawer, string operationId, string fileName, CancellationToken cancellationToken
        )
        {
            while (true)
            {
                var operation = await diskClient.GetOperationStatus(operationId, cancellationToken);
                drawer.SetUploadStatus(fileName, operation.GetStatus());
                if (operation.IsCompleted() || cancellationToken.IsCancellationRequested)
                {
                    break;
                }

                await Task.Delay(waitingDelay, cancellationToken);
            }
        }

        private static async Task UploadFileAsync(
            IYandexDiskClient diskClient, IUploadProgressDrawer drawer, FileInfo file, string destinationUri, CancellationToken cancellationToken)
        {
            drawer.SetUploadStatus(file.Name, OperationStatus.InProgress);

            try
            {
                var link = await diskClient.GetUploadLinkAsync($"{destinationUri}/{file.Name}", true, cancellationToken);
                await using var stream = File.OpenRead(file.FullName);
                await diskClient.UploadFileAsync(link, stream, cancellationToken);
                await WaitOperationAsync(diskClient, drawer, link.OperationId, file.Name, cancellationToken);
            }
            catch (YandexDiskException e)
            {
                drawer.SetUploadStatus(file.Name, OperationStatus.Failure, e.Error);
            }
        }

        private static async Task UploadFilesAsync(
            IYandexDiskClient diskClient, IUploadProgressDrawer drawer, string directoryPath, string destinationUri)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            var token = cancellationTokenSource.Token;

            var uploadTasks = new DirectoryInfo(directoryPath)
                .GetFiles()
                .Select(async file => await UploadFileAsync(diskClient, drawer, file, destinationUri, token));

            await Task.WhenAll(uploadTasks); 
        }

        private static (string directoryPath, string uriString, string token) ParseArgs(string[] args)
        {
            if (args.Length < 3)
            {
                throw new ArgumentException("First argument must be the source folder; " +
                                            "second argument must contain the correct address of yandex disk folder; " +
                                            "third argument must be the correct Yandex.OAuth token", nameof(args));
            }

            var directoryPath = args[0];
            if (!Directory.Exists(directoryPath))
            {
                throw new ArgumentException($"Path {directoryPath} doesn't exist", nameof(args));
            }

            var uriString = args[1];
            var token = args[2];

            return (directoryPath, uriString, token);
        }

        static async Task Main(string[] args)
        {
            try
            {
                var (directoryPath, uriString, token) = ParseArgs(args);
                var diskClient = new YandexDiskClient(token);
                var consoleDrawer = new ConsoleDrawer();
                await UploadFilesAsync(diskClient, consoleDrawer, directoryPath, uriString);
            }
            catch (Exception e)
            {
                Console.WriteLine($"An error occurred: {e.Message}");
                return;
            }
        }
    }
}
